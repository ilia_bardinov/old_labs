#include "big_numbers.h"

void initCalculatingSystem()
{
    bool bStart = true;
    std::string strOutput;
    char outputSign = 1;
    strOutput = "";
    std::cout << "Calculating System start. Enter numbers and operators with ';' in the end => 'X * Y + Z;' : ";
    while(bStart) {
        std::string strFirstOperand, strSecondOperand;
        char operatorSign, FirstOperandSign, SecondOperandSign;
        if(strOutput == "")
            std::cin >> strFirstOperand >> operatorSign >> strSecondOperand;
        else {
            std::cin >> operatorSign >> strSecondOperand;
            strFirstOperand = outputSign + strOutput;
        }
        /* parsing numbers for error and calculating => begin */
        std::cout << std::endl << "parsing......" << std::endl;
        if(strFirstOperand[0] == '-') {
            FirstOperandSign = '-';
            strFirstOperand.erase(strFirstOperand.begin());
        } else
            FirstOperandSign = 1;
        if(strSecondOperand[0] == '-') {
            SecondOperandSign = '-';
            strSecondOperand.erase(strSecondOperand.begin());
        } else
            SecondOperandSign = 1;
        uint64 maxArraySize = strFirstOperand.size() > strSecondOperand.size() ? strFirstOperand.size() : strSecondOperand.size();
        for(int i = 0; i < maxArraySize; i++) {
            if(!strFirstOperand[i])
                strFirstOperand.insert(strFirstOperand.begin(), '0');
            if(!strSecondOperand[i])
                strSecondOperand.insert(strSecondOperand.begin(), '0');
            if(strFirstOperand[i] != '1' && strFirstOperand[i] != '2' && strFirstOperand[i] != '3' && strFirstOperand[i] != '4' && strFirstOperand[i] != '5'
               && strFirstOperand[i] != '6' && strFirstOperand[i] != '7' && strFirstOperand[i] != '8' && strFirstOperand[i] != '9' && strFirstOperand[i] != '0') {
                std::cout << "warning: invalid character in '" << strFirstOperand << "' => '" << strFirstOperand[i] << "' => erase character from string" << std::endl;
                strFirstOperand.erase(strFirstOperand.begin()+i);
                i--;
                continue;
            }
            if(strSecondOperand[i] != '1' && strSecondOperand[i] != '2' && strSecondOperand[i] != '3' && strSecondOperand[i] != '4' && strSecondOperand[i] != '5'
               && strSecondOperand[i] != '6' && strSecondOperand[i] != '7' && strSecondOperand[i] != '8' && strSecondOperand[i] != '9' && strSecondOperand[i] != '0') {
                if(strSecondOperand[i] != ';')
                    std::cout << "warning: invalid character in '" << strSecondOperand << "' => '" << strSecondOperand[i] << "' => erase character from string" << std::endl;
                else {
                    std::cout << "attention: meet end-line signal in '" << strSecondOperand << "' => '" << strSecondOperand[i] << "' => last number, erase from string" << std::endl;
                    bStart = false;
                }
                strSecondOperand.erase(strSecondOperand.begin()+i);
                i--;
                continue;
            }
        }
        while(strFirstOperand[0] == '0' && strSecondOperand[0] == '0') {
            std::cout << "founded unnecessary '" << strFirstOperand[0] << "' in '" << strFirstOperand <<"' and '" << strSecondOperand[0] << "' in '" << strSecondOperand << "' => erase it => '";
            strFirstOperand.erase(strFirstOperand.begin());
            strSecondOperand.erase(strSecondOperand.begin());
            std::cout << strFirstOperand << "' and '" << strSecondOperand << "'" << std::endl;
        }
        /* parsing numbers for error and calculating => end */
        outputSign = 1;
        std::cout << std::endl << "calculating......" << std::endl;
        switch(operatorSign) {
            case '+':
                if(FirstOperandSign != '-' && SecondOperandSign != '-')
                    strOutput = calculateSum(strFirstOperand, strSecondOperand);
                else if(FirstOperandSign != '-' && SecondOperandSign == '-')
                    strOutput = calculateDiff(strFirstOperand, strSecondOperand, &outputSign);
                else if(FirstOperandSign == '-' && SecondOperandSign != '-') {
                    strOutput = calculateDiff(strFirstOperand, strSecondOperand, &outputSign);
                    if(outputSign == '-')
                        outputSign = 1;
                    else
                        outputSign = '-';
                } else if(FirstOperandSign == '-' && SecondOperandSign == '-') {
                    strOutput = calculateSum(strFirstOperand, strSecondOperand);
                    outputSign = '-';
                }
                break;
            case '-':
                if(FirstOperandSign != '-' && SecondOperandSign != '-')
                    strOutput = calculateDiff(strFirstOperand, strSecondOperand, &outputSign);
                else if(FirstOperandSign != '-' && SecondOperandSign == '-')
                    strOutput = calculateSum(strFirstOperand, strSecondOperand);
                else if(FirstOperandSign == '-' && SecondOperandSign != '-') {
                    strOutput = calculateSum(strFirstOperand, strSecondOperand);
                    outputSign = '-';
                } else if(FirstOperandSign == '-' && SecondOperandSign == '-') {
                    strOutput = calculateDiff(strFirstOperand, strSecondOperand, &outputSign);
                    if(outputSign == '-')
                        outputSign = 1;
                    else
                        outputSign = '-';
                }
                break;
            case '*':
                strOutput = calculateMultiplication(strFirstOperand, strSecondOperand);
                if((FirstOperandSign != '-' && SecondOperandSign == '-') || (FirstOperandSign == '-' && SecondOperandSign != '-'))
                    outputSign = '-';
                break;
            case '/':
                strOutput = calculateDivision(strFirstOperand, strSecondOperand);
                if((FirstOperandSign != '-' && SecondOperandSign == '-') || (FirstOperandSign == '-' && SecondOperandSign != '-'))
                    outputSign = '-';
                break;
            default :
                std::cout << "error: unknown operator sign => '" << operatorSign << "'" << std::endl;
                std::cout << "Some errors occured" << std::endl;
                return;
        }
        while(strFirstOperand[0] == '0')
            strFirstOperand.erase(strFirstOperand.begin());
        while(strSecondOperand[0] == '0')
            strSecondOperand.erase(strSecondOperand.begin());
        while(strOutput[0] == '0')
            strOutput.erase(strOutput.begin());
        if(strFirstOperand == "") {
            strFirstOperand = "0";
            FirstOperandSign = 1;
        }
        if(strSecondOperand == "") {
            strSecondOperand = "0";
            FirstOperandSign = 1;
        }
        if(strOutput == "") {
            strOutput = "0";
            outputSign = 1;
        }

        std::cout << std::endl << ((FirstOperandSign == 1) ? "" : "-") << strFirstOperand << " " << operatorSign << " ";
        std::cout << ((SecondOperandSign == 1) ? "" : "-") << strSecondOperand << " = " << ((outputSign == 1) ? "" : "-") << strOutput << std::endl;
    }
}

std::string calculateSum(std::string strFirstOperand, std::string strSecondOperand)
{
    std::string strOutput;
    bool bNext = false;
    for(int i = strFirstOperand.size()-1; i >= 0; i--) {
        char sum = strFirstOperand[i] - '0' + strSecondOperand[i] - '0';
        if(bNext) {
            sum += 1;
            bNext = false;
        }
        if(sum >= 10) {
            char modSum = sum%10 + '0';
            strOutput.insert(strOutput.begin(), modSum);
            bNext = true;
        } else {
            strOutput.insert(strOutput.begin(), sum + '0');
        }
    }
    if(bNext)
        strOutput.insert(strOutput.begin(), '1');
    return strOutput;
}

std::string calculateDiff(std::string strFirstOperand, std::string strSecondOperand, char *outputSign)
{
    std::string strOutput;
    bool bNext = false;
    for(int i = 0; i < strFirstOperand.size(); i++) {
        if(strFirstOperand[i] > strSecondOperand[i]) {
            break;
        } else if(strSecondOperand[i] > strFirstOperand[i]) {
            swap(strFirstOperand, strSecondOperand);
            (*outputSign) = '-';
            break;
        }
    }
    for(int i = strFirstOperand.size()-1; i >= 0; i--) {
        char diff = (strFirstOperand[i] - '0') - (strSecondOperand[i] - '0');
        if(bNext) {
            diff -= 1;
            bNext = false;
        }
        if(diff < 0) {
            char modDiff = 10 + diff + '0';
            strOutput.insert(strOutput.begin(), modDiff);
            bNext = true;
        } else {
            strOutput.insert(strOutput.begin(), diff + '0');
        }
    }
    return strOutput;
}

std::string calculateMultiplication(std::string str1, std::string str2)
{
    std::string strOutput;
    std::vector <std::string> stringArray;
    uint64 multi = 0, div = 0, count = 0;
    std::string sum;
    for(int64 i = str2.size()-1; i >= 0; i--) {
        for(uint64 k = 0; k < count; k++)
            sum.insert(sum.begin(), '0');
        for(int64 j = str1.size()-1; j >= 0; j--) {
            multi = (str1[j] - '0') * (str2[i] - '0') + div;
            div = multi / 10;
            unsigned char mod = (multi % 10) + '0';
            sum.insert(sum.begin(), mod);
        }
        if(div > 0) {
            sum.insert(sum.begin(), div + '0');
            div = 0;
        }
        for(int64 k = 0; k < i; k++)
            sum.insert(sum.begin(), '0');
        stringArray.push_back(sum);
        sum = "";
        count++;
    }
    multi = 0;
    div = 0;
    for(int64 i = stringArray[0].size()-1; i >= 0; i--) {
        for(uint64 j = 0; j < stringArray.size(); j++) {
            multi += stringArray[j][i] - '0';
        }
        if(div > 0) {
            multi += div;
            div = 0;
        }
        div = multi / 10;
        unsigned char mod = (multi % 10) + '0';
        strOutput.insert(strOutput.begin(), mod);
        multi = 0;
    }

    return strOutput;
}

std::string calculateDivision(std::string str1, std::string str2)
{
    std::string strOutput;
    for(uint64 i = 0; i < str1.size(); i++) {
        if(str1[i] > str2[i]) {
            break;
        } else if(str2[i] > str1[i]) {
            strOutput = "0";
            return strOutput;
        }
    }
    uint64 count = 0;
    for(uint64 i = 0; i < str2.size(); i++)
        if(str2[i] == '0')
            count++;
    if(count == str2.size()) {
        strOutput = "division by 0 => value is not defined";
        return strOutput;
    }
    std::string tmpDiv("");
    for(uint64 i = 0; i < str1.size(); i++) {
        if(tmpDiv != "") {
            char sign = 1;
            uint64 maxArraySize = tmpDiv.size() > str2.size() ? tmpDiv.size() : str2.size();
            for(uint64 j = 0; j < maxArraySize; j++) {
                if(!tmpDiv[j])
                    tmpDiv.insert(tmpDiv.begin(), '0');
                if(!str2[j])
                    str2.insert(str2.begin(), '0');
            }
            std::string different = calculateDiff(tmpDiv, str2, &sign);
            if(sign == '-') {
                tmpDiv.insert(tmpDiv.end(), str1[i]);
                strOutput.insert(strOutput.end(), '0');
            } else {
                char value = 1;
                tmpDiv = different;
                while(sign != '-') {
                    uint64 maxArraySize = tmpDiv.size() > str2.size() ? tmpDiv.size() : str2.size();
                    for(int j = 0; j < maxArraySize; j++) {
                        if(!tmpDiv[j])
                            tmpDiv.insert(tmpDiv.begin(), '0');
                        if(!str2[j])
                            str2.insert(str2.begin(), '0');
                    }
                    sign = 1;
                    different = calculateDiff(tmpDiv, str2, &sign);
                    if(sign != '-') {
                        tmpDiv = different;
                        value++;
                    }
                }
                strOutput.insert(strOutput.end(), value + '0');
                tmpDiv.insert(tmpDiv.end(), str1[i]);
            }
        } else {
            tmpDiv.insert(tmpDiv.end(), str1[i]);
        }
    }
    if(tmpDiv != "") {
        char sign = 1;
        uint64 maxArraySize = tmpDiv.size() > str2.size() ? tmpDiv.size() : str2.size();
        for(uint64 j = 0; j < maxArraySize; j++) {
            if(!tmpDiv[j])
                tmpDiv.insert(tmpDiv.begin(), '0');
            if(!str2[j])
                str2.insert(str2.begin(), '0');
        }
        std::string different = calculateDiff(tmpDiv, str2, &sign);
        if(sign == '-')
            strOutput.insert(strOutput.end(), '0');
        else {
            char value = 1;
            tmpDiv = different;
            while(sign != '-') {
                uint64 maxArraySize = tmpDiv.size() > str2.size() ? tmpDiv.size() : str2.size();
                for(int j = 0; j < maxArraySize; j++) {
                    if(!tmpDiv[j])
                        tmpDiv.insert(tmpDiv.begin(), '0');
                    if(!str2[j])
                        str2.insert(str2.begin(), '0');
                }
                sign = 1;
                different = calculateDiff(tmpDiv, str2, &sign);
                if(sign != '-') {
                    tmpDiv = different;
                    value++;
                }
            }
            strOutput.insert(strOutput.end(), value + '0');
        }
    }
    return strOutput;
}
