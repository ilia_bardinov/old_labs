#include "matrix.h"

Matrix::Matrix(const int line, const int column)
{
    m_line = line;
    m_column = column;
    m_matrix = new double *[line];
    for(int j = 0; j < line; j++) {
        m_matrix[j] = new double[column];
        for(int i = 0; i < column; i++)
            m_matrix[j][i] = 0;
    }
}

Matrix::Matrix(const int line, const int column, const double *a)
{
    m_line = line;
    m_column = column;
    m_matrix = new double *[line];
    int k = 0;
    for(int j = 0; j < line; j++) {
        m_matrix[j] = new double[column];
        for(int i = 0; i < column; i++) {
            m_matrix[j][i] = a[k];
            k++;
        }
    }
}

Matrix::~Matrix()
{
    for(int i = 0; i < m_line; i++)
        delete []m_matrix[i];
    delete []m_matrix;
}

const Matrix Matrix::operator + (const Matrix &a) const
{
    Matrix pMatrix(m_line, m_column);
    if(m_line == a.m_line && m_column == a.m_column) {
        for(int j = 0; j < m_line; j++) {
            for(int i = 0; i < m_column; i++)
                pMatrix.m_matrix[j][i] = m_matrix[j][i] + a.m_matrix[j][i];
        }
    }
    return pMatrix;
}

const void Matrix::operator += (const Matrix &a)
{
    if(m_line == a.m_line && m_column == a.m_column) {
        for(int j = 0; j < m_line; j++) {
            for(int i = 0; i < m_column; i++)
                m_matrix[j][i] += a.m_matrix[j][i];
        }
    }
}

const Matrix Matrix::operator - (const Matrix &a) const
{
    Matrix pMatrix(m_line, m_column);
    if(m_line == a.m_line && m_column == a.m_column) {
        for(int j = 0; j < m_line; j++) {
            for(int i = 0; i < m_column; i++)
                pMatrix.m_matrix[j][i] = m_matrix[j][i] - a.m_matrix[j][i];
        }
    }
    return pMatrix;
}

const void Matrix::operator -= (const Matrix &a)
{
    if(m_line == a.m_line && m_column == a.m_column) {
        for(int j = 0; j < m_line; j++) {
            for(int i = 0; i < m_column; i++)
                m_matrix[j][i] -= a.m_matrix[j][i];
        }
    }
}

const Matrix Matrix::operator * (const double a) const
{
    Matrix pMatrix(m_line, m_column);
    for(int j = 0; j < m_line; j++) {
        for(int i = 0; i < m_column; i++)
            pMatrix.m_matrix[j][i] = m_matrix[j][i] * a;
    }
    return pMatrix;
}

const void Matrix::operator *= (const double a)
{
    for(int j = 0; j < m_line; j++) {
        for(int i = 0; i < m_column; i++)
            m_matrix[j][i] *= a;
    }
}

const Matrix Matrix::operator * (const Matrix &a) const
{
    Matrix pMatrix(m_line, a.m_column);
    if(m_column == a.m_line)
        for(int j = 0; j < m_line; j++)
            for(int i = 0; i < a.m_column; i++)
                for(int k = 0; k < m_column; k++)
                    pMatrix.m_matrix[j][i] += (m_matrix[j][k] * a.m_matrix[k][i]);
    return pMatrix;
}

const void Matrix::operator *= (const Matrix &a)
{
    Matrix pMatrix(m_line, a.m_column);
    if(m_column == a.m_line)
        for(int j = 0; j < m_line; j++)
            for(int i = 0; i < a.m_column; i++)
                for(int k = 0; k < m_column; k++)
                    pMatrix.m_matrix[j][i] += (m_matrix[j][k] * a.m_matrix[k][i]);
    *this = pMatrix;
}

const Vector Matrix::operator [](int i) const
{
    if(i <= 0)
        i = 1;
    Vector vector(m_line, m_matrix[i-1]);
    return vector;
}

Matrix & Matrix::operator = (const Matrix &a)
{
    if(this == &a)
        return *this;

    for(int i = 0; i < m_line; i++)
        delete []m_matrix[i];
    delete []m_matrix;

    m_line = a.m_line;
    m_column = a.m_column;

    m_matrix = new double *[m_line];
    for(int j = 0; j < m_line; j++) {
        m_matrix[j] = new double[m_column];
        for(int i = 0; i < m_column; i++)
            m_matrix[j][i] = a.m_matrix[j][i];
    }
    return *this;
}

Vector Matrix::toVector() const
{
    double *tmp = new double[m_line*m_column];
    for(int j = 0; j < m_line; j++)
        for(int i = 0; i < m_column; i++)
            tmp[j*m_column+i] = m_matrix[j][i];
    Vector vector(m_line*m_column, tmp);
    return vector;
}

void Matrix::print() const
{
    for(int j = 0; j < m_line; j++) {
        for(int i = 0; i < m_column; i++) {
            std::cout << m_matrix[j][i] << " ";
        }
        std::cout << std::endl;
    }
}

Vector::Vector(const int line)
{
    m_line = line;
    m_vector = new double [line];
    for(int j = 0; j < line; j++)
        m_vector[j] = 0;
}

Vector::Vector(const int line, const double *a)
{
    m_line = line;
    m_vector = new double [line];
    for(int j = 0; j < line; j++)
        m_vector[j] = a[j];
}

Vector::~Vector()
{
    delete []m_vector;
}

const Vector Vector::operator + (const Vector &a) const
{
    Vector vector(m_line);
    if(m_line == a.m_line)
        for(int j = 0; j < m_line; j++)
            vector.m_vector[j] = m_vector[j] + a.m_vector[j];
    return vector;
}

const void Vector::operator += (const Vector &a)
{
    if(m_line == a.m_line)
        for(int j = 0; j < m_line; j++)
            m_vector[j] += a.m_vector[j];
}

const Vector Vector::operator - (const Vector &a) const
{
    Vector vector(m_line);
    if(m_line == a.m_line)
        for(int j = 0; j < m_line; j++)
            vector.m_vector[j] = m_vector[j] - a.m_vector[j];
    return vector;
}

const void Vector::operator -= (const Vector &a)
{
    if(m_line == a.m_line)
        for(int j = 0; j < m_line; j++)
            m_vector[j] -= a.m_vector[j];
}

const Vector Vector::operator * (const double a) const
{
    Vector vector(m_line);
    for(int j = 0; j < m_line; j++)
        vector.m_vector[j] = m_vector[j] * a;
    return vector;
}

const void Vector::operator *= (const double a)
{
    for(int j = 0; j < m_line; j++)
        m_vector[j] *= a;
}

Vector & Vector::operator = (const Vector &a)
{
    if(this == &a)
        return *this;

    delete []m_vector;

    m_line = a.m_line;

    m_vector = new double [m_line];
    for(int j = 0; j < m_line; j++)
        m_vector[j] = a.m_vector[j];
    return *this;
}

const double Vector::operator [](const int j) const
{
    if(j <= 0)
        return 0;
    return m_vector[j-1];
}

void Vector::print() const
{
    for(int j = 0; j < m_line; j++)
        std::cout << m_vector[j] << " ";
    std::cout << std::endl;
}
