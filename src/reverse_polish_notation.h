#include "pch.h"

class Stack
{
    public:
        void push(int64 elem); // add element at the end
        void pop(); // remove last element
        int64 top(); // get value of last element
        int64 getValue(uint8 i); // get value of element with index i
        uint8 getStackSize(); // get size of vector
        void clear(); // remove all elements in vector
    private:
        std::vector <int64> m_elem;    // vector
};

void startStack(Stack *pStack); // it is a test of stack
void reversePolishNotation(); // realization of reverse polish notation
