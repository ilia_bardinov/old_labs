#include "singly_linked_list.h"

SL_List *createSLListFromFile(std::string filename)
{
    std::ifstream pFile;
    pFile.open(filename);
    if(!pFile) {
        std::cout << filename << " => file read error" << std::endl;
        return nullptr;
    }

    SL_List *pList = nullptr;
    SL_List *pFirstElement;
    std::string str;

    while(!pFile.eof()) {
        pFile >> str;
        const char *cstr = str.c_str();
        int64 value = atoll(cstr);
        //std::cout << str << ", ";
        if(!pList) {
            pList = new SL_List();
            pList->num = value;
            pList->pNext = nullptr;
            pFirstElement = pList;
        } else {
            SL_List *pTmpList = new SL_List();
            pList->pNext = pTmpList;
            pTmpList->num = value;
            pList = pTmpList;
        }
    }
    pFile.close();
    return pFirstElement;
}

void outputSLList(SL_List *pFirstElement)
{
    while(pFirstElement) {
        std::cout << pFirstElement->num << ", ";
        pFirstElement = pFirstElement->pNext;
    }
    std::cout << std::endl;
}

void deleteSLList(SL_List *pFirstElement)
{
    SL_List *pTmpList = nullptr;
    while(pFirstElement) {
        pTmpList = pFirstElement;
        pFirstElement = pFirstElement->pNext;
        delete pTmpList;
    }
    pFirstElement = nullptr;
}

void sortingSLList(SL_List *pFirstElement, char type)
{
    if(type != '>' && type != '<') {
        std::cout << type << " => found error in 'type' of 'void sortingSLList(SL_List *pFirstElement, char type)'" << std::endl;
        return;
    }

    while(pFirstElement) {
        SL_List *pTmplist = pFirstElement;
        while(pTmplist) {
            if(type == '<') {
                if(pFirstElement->num < pTmplist->num)
                    std::swap(pFirstElement->num,pTmplist->num);
            } else if(type == '>') {
                if(pFirstElement->num > pTmplist->num)
                    std::swap(pFirstElement->num,pTmplist->num);
            }
            pTmplist = pTmplist->pNext;
        }
        pFirstElement = pFirstElement->pNext;
    }
}
