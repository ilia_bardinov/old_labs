#include "pch.h"

struct SL_List {
    int64 num;
    SL_List *pNext;
};

SL_List *createSLListFromFile(std::string filename); // create new list with pointer to first element, data from text file
void outputSLList(SL_List *pFirstElement); // print list from pFirstElement
void deleteSLList(SL_List *pFirstElement); // set list to be empty
void sortingSLList(SL_List *pFirstElement, char type); // sorting list from low to high or high to low
